jQuery(document).ready(function ($) {

    /**
     * Header
     */
    $('.navbar-toggle').click(function () {
        if ($('.icon-bar1').hasClass('active')) {
            $('.icon-bar1').removeClass('active');
            $('.icon-bar3').removeClass('active');
            $(".sidenav-overlay").removeClass('active');

            $(".sidenav").css("width", "0");

            setTimeout(function () {
                $('.icon-bar2').removeClass('active');
            }, 300)
        } else {
            $(".sidenav-overlay").addClass('active');

            $(".sidenav").css("width", "80%");
            $('.icon-bar1').addClass('active');
            $('.icon-bar2').addClass('active');
            $('.icon-bar3').addClass('active');
        }
    });
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll > 50) {
            $('.sidenav').css('top', 0);
        } else {
            $('.sidenav').css('top', '50px');
        }
    });


    /**
     * jQuery function to prevent default anchor event and take the href * and the title to make a share popup
     *
     * @param  {[object]} e           [Mouse event]
     * @param  {[integer]} intWidth   [Popup width defalut 500]
     * @param  {[integer]} intHeight  [Popup height defalut 400]
     * @param  {[boolean]} blnResize  [Is popup resizeabel default true]
     */
    $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {

        // Prevent default anchor event
        e.preventDefault();

        // Set values for window
        intWidth = intWidth || '500';
        intHeight = intHeight || '400';
        strResize = (blnResize ? 'yes' : 'no');

        // Set title and open popup with focus on it
        var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
            strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,
            objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
    };

    /* ================================================== */

    $('.customer.share').on("click", function (e) {
        $(this).customerPopup(e);
    });

    /**
     *  Google Maps
     */
    function initMap() {
        if ($("#map").length) {

            var uluru = {lat: 51.555609, lng: 5.682674};
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 51.755609, lng: 5.682674},
                zoom: 9
            });

            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });

            var Infocontent =
                '<section id="infobox"> ' +
                '<label class="marker-icon"></label>' +
                '<h2 class="title font-md">Hoofdkantoor a. van schijndel beheer</h2>' +
                '<div class="adres">' +
                '<p class="font-sm">Gelind 2, 5421 BN Gemert</p>' +
                '</div>' +
                '<a href="https://www.google.nl/maps/place/Gelind+2,+5421+BN+Gemert/@51.5552723,5.6821507,18.5z/data=!4m5!3m4!1s0x47c718a4a39c71cd:0x7f226e7df4a68d90!8m2!3d51.555609!4d5.682674?hl=en" target="_blank" class="btn-marker">Krijg route</a> ' +
                '</section>';


            var ibOptions = {
                content: Infocontent,
                disableAutoPan: false,
                pixelOffset: new google.maps.Size(-130, -300),
                closeBoxURL: "",
                infoBoxClearance: new google.maps.Size(1, 1),
                pane: "floatPane",
            };
            infobox = new InfoBox(ibOptions);
            google.maps.event.addListenerOnce(map, 'idle', function () {
                infobox.open(map, marker);
            });
        }
    }

    initMap();

    /**
     *  Sidebar verkoop en verhuur
     */
    var desktop = true;

    $(window).resize(function () {
        mobilizeSidebar();
    });

    function mobilizeSidebar() {
        if ($(window).width() < 778) {
            if (desktop) {
                desktop = false;
                var el = $("#mobile-sidebar-cut").detach();
                $("#mobile-sidebar-paste").append(el);
            }
        }
        if ($(window).width() >= 778) {
            if (!desktop) {
                desktop = true;
                var el = $("#mobile-sidebar-cut").detach();
                $("#mobile-sidebar-cut2").append(el);
            }
        }
    }

    mobilizeSidebar();

    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 4000,
        slideSpeed: 300,
        paginationSpeed: 400,
        items: 1,
        pagination: false,
        navigation: true,
        autoHeight: false,
        autoHeightClass: 'image-wrapper'
    });

});


