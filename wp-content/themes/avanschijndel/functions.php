<?php
/**
 * avanschijndel functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package avanschijndel
 */
if ( ! function_exists( 'avanschijndel_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function avanschijndel_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on avanschijndel, use a find and replace
		 * to change 'avanschijndel' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'avanschijndel', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'avanschijndel' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'avanschijndel_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'avanschijndel_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function avanschijndel_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'avanschijndel_content_width', 640 );
}

add_action( 'after_setup_theme', 'avanschijndel_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function avanschijndel_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'First Footer Widget Area', 'widget-vince-footer' ),
		'id'            => 'first-footer-widget-area',
		'description'   => __( 'The first footer widget area', 'tutsplus' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	// Second Footer Widget Area, located in the footer. Empty by default.
	register_sidebar( array(
		'name'         => __( 'Second Footer Widget Area', 'widget-schijndel-footer' ),
		'id'           => 'second-footer-widget-area',
		'description'  => __( 'The second footer widget area', 'vince' ),
		'before_title' => '<h3 class="font-md bold">',
		'after_title'  => '</h3>',
	) );

	// Third Footer Widget Area, located in the footer. Empty by default.
	register_sidebar( array(
		'name'         => __( 'Third Footer Widget Area', 'widget-schijndel-footer' ),
		'id'           => 'third-footer-widget-area',
		'description'  => __( 'The third footer widget area', 'vince' ),
		'before_title' => '<h3 class="font-md bold">',
		'after_title'  => '</h3>',
	) );

	// Fourth Footer Widget Area, located in the footer. Empty by default.
	register_sidebar( array(
		'name'        => __( 'Fourth Footer Widget Area', 'widget-schijndel-footer' ),
		'id'          => 'fourth-footer-widget-area',
		'description' => __( 'The fourth footer widget area', 'vince' ),
	) );

}

add_action( 'widgets_init', 'avanschijndel_widgets_init' );

function avanschijndel_set_php_js_variables() {
	$passedValues = array(
		'wp_is_mobile' => wp_is_mobile(),
	);


	wp_localize_script( 'app.js', 'php_variables', $passedValues );
}

/**
 * Enqueue scripts and styles.
 */
function avanschijndel_scripts() {
	wp_enqueue_style( 'theme.less', get_template_directory_uri() . '/css/dist/theme.css' );
	wp_register_script( 'google-maps', '//maps.googleapis.com/maps/api/js?key=AIzaSyAcM2Ekv3_FwhFl9QAQSjjbmjbnFIqifcw', null, null, true );
	wp_enqueue_script( 'google-maps' );
	wp_register_script( 'app.js', get_template_directory_uri() . '/js/dist/le-gulp-javascript.min.js', array( 'jquery' ), '1.0.0', true );
	avanschijndel_set_php_js_variables();
	wp_enqueue_script( 'app.js' );

}

add_action( 'wp_enqueue_scripts', 'avanschijndel_scripts' );

// remove header links
function avanschijndel_head_cleanup() {
	// REMOVE WP EMOJI
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );

	remove_action( 'template_redirect', 'rest_output_link_header', 11 );
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
	remove_action( 'wp_head', 'rest_output_link_wp_head' );


	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	remove_action( 'wp_head', 'rsd_link' ); //removes EditURI/RSD (Really Simple Discovery) link.
	remove_action( 'wp_head', 'feed_links_extra', 3 );                      // Category Feeds
	remove_action( 'wp_head', 'feed_links', 2 );                            // Post and Comment Feeds
	remove_action( 'wp_head', 'rsd_link' );                                 // EditURI link
	remove_action( 'wp_head', 'wlwmanifest_link' );                         // Windows Live Writer
	remove_action( 'wp_head', 'index_rel_link' );                           // index link
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'wp_resource_hints', 2 );
// WP version
}

add_action( 'init', 'avanschijndel_head_cleanup' );

function avanschijndel_active_menu_class( $classes, $item ) {
	if ( in_array( 'current-menu-item', $classes ) ) {
		$classes[] = 'active ';
	}
	if ( strpos( $_SERVER['REQUEST_URI'], $item->post_name ) ) {
		$classes[] = 'active ';
	}

	return $classes;
}

add_filter( 'nav_menu_css_class', 'avanschijndel_active_menu_class', 10, 2 );

function avanschijndel_change_post_label() {
	global $menu;
	global $submenu;
	$menu[5][0]                 = 'Nieuws';
	$submenu['edit.php'][5][0]  = 'Nieuws';
	$submenu['edit.php'][10][0] = 'Add Nieuws';
	$submenu['edit.php'][16][0] = 'Nieuws Tags';
}

function avanschijndel_change_post_object() {
	global $wp_post_types;
	$labels                     = &$wp_post_types['post']->labels;
	$labels->name               = 'nieuws';
	$labels->singular_name      = 'Nieuws';
	$labels->add_new            = 'Add Nieuws';
	$labels->add_new_item       = 'Add Nieuws';
	$labels->edit_item          = 'Edit Nieuws';
	$labels->new_item           = 'Nieuws';
	$labels->view_item          = 'View Nieuws';
	$labels->search_items       = 'Search Nieuws';
	$labels->not_found          = 'No Nieuws found';
	$labels->not_found_in_trash = 'No Nieuws found in Trash';
	$labels->all_items          = 'All Nieuws';
	$labels->menu_name          = 'Nieuws';
	$labels->name_admin_bar     = 'Nieuws';
}

add_action( 'admin_menu', 'avanschijndel_change_post_label' );
add_action( 'init', 'avanschijndel_change_post_object' );


// This theme uses wp_nav_menu() in one location.
register_nav_menus( array(
	'primary' => 'Primary Menu',
) );

function avanschijndel_og_data_in_head() {
	global $post;
	if ( ! is_singular() ) //if it is not a post or a page
	{
		return;
	}
	echo '<meta property="og:title" content="' . get_the_title() . '"/>';
	echo '<meta property="og:type" content="article"/>';
	echo '<meta property="og:url" content="' . get_permalink() . '"/>';
	echo '<meta property="og:description" content="' . get_the_content() . '"/>';
	echo '<meta property="og:site_name" content="a van schijndelbeheer"/>';
	if ( ! has_post_thumbnail( $post->ID ) ) { //the post does not have featured image, use a default image
		$default_image = get_template_directory_uri() . "/images/schijndellogo.png"; //replace this with a default image on your server or an image in your media library
		echo '<meta property="og:image" content="' . $default_image . '" itemprop="thumbnailUrl"/>';
	} else {
		$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
		echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '" itemprop="thumbnailUrl"/>';
	}
	echo "
";
}

add_action( 'wp_head', 'avanschijndel_og_data_in_head', 5 );


function avanschijndel_get_breadcrumb_link( $parent_page_title ) {
	$page = get_page_by_title( $parent_page_title );

	return get_permalink( $page->ID );
}

function avanschijndel_get_single_taxonomy( $id, $tax ) {
	$taxonomy = wp_get_post_terms( $id, $tax );

	return $taxonomy;
}

function avanschijndel_post_slugs( $post_link, $id = 0 ) {
	$post = get_post( $id );
	if ( is_object( $post ) ) {
		$terms = wp_get_object_terms( $post->ID, 'Sector' );
		if ( $terms ) {
			return str_replace( '%Sector%', $terms[0]->slug, $post_link );
		}
	}

	return $post_link;
}

add_filter( 'post_type_link', 'avanschijndel_post_slugs', 1, 3 );

/**
 *  Admin verkoop page
 */
include( 'php/admin_verkoop.php' );
/**
 *  Admin verhuur page
 */
include( 'php/admin_verhuur.php' );
/**
 *  Admin taxonomies
 */
include( 'php/admin_taxonomy.php' );






