<section class="admin-custom-metabox">
    <div class="row">
        <div class="col-md-12">
            <section id="dynamic_verhuur_metabox_wrapper">
				<?php
				if ( ! empty( $metadata ) ) {
					foreach ( $metadata as $key => $value ) {
						if ( ! empty( $value ) ) { ?>
                            <section class="dynamic-input">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label class=" font-md">Titel</label>
                                            <input type="text" class="form-control verhuur_available"
                                                   name="verhuur_available[<?php echo $key ?>][title]"
                                                   maxlength="30"
                                                   value="<?php echo $value['title'] ?>">
                                            <label class=" font-md">Beschikbaar</label>
                                            <input type="text" class="form-control"
                                                   name="verhuur_available[<?php echo $key ?>][beschikbaar]"
                                                   placeholder="Bijv. vanaf 28 december 2018"
                                                   maxlength="40"
                                                   value="<?php echo $value['beschikbaar'] ?>">
                                            <div class="form-group">
                                                <label class="font-md">Beschrijving</label>
                                                <textarea class="form-control"
                                                          name="verhuur_available[<?php echo $key ?>][beschrijving]"><?php echo $value['beschrijving'] ?></textarea>
                                            </div>
                                            <label class="font-md">Foto gallerij</label>
                                            <p class="pretty-text">Klik op <b>media toevoegen</b> > <b>Galerij
                                                    aanmaken</b> > <b>e.v.t. bestanden uploaden</b> > <b>Bestanden
                                                    selecteren</b></p>
											<?php
											$content  = $value['images'];
											$editor   = 'verhuur_available' . $key;
											$settings = array(
												'textarea_name' => 'verhuur_available[' . $key . '][images]',
												'textarea_rows' => 8,
												'media_buttons' => true,
												'teeny'         => true,
											);
											wp_editor( $content, $editor, $settings ); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-danger delete-input" href="" role="button">
                                            <span class="glyphicon glyphicon-minus"></span>
                                        </button>
                                    </div>
                                </div>
                            </section>
						<?php }
					}
				} else {
					$key = 0;
					?>
                    <section class="dynamic-input">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label class="font-md">Titel</label>
                                    <input type="text" class="form-control verhuur_available"
                                           name="verhuur_available[0][title]"
                                           maxlength="30"
                                           value="">
                                    <label class=" font-md">Beschikbaar</label>
                                    <input type="text" class="form-control"
                                           name="verhuur_available[0][beschikbaar]"
                                           placeholder="Bijv. vanaf 28 december 2018"
                                           maxlength="40"
                                           value="">
                                    <div class="form-group">
                                        <label class="font-md">Beschrijving</label>
                                        <textarea class="form-control"
                                                  name="verhuur_available[0][beschrijving]"></textarea>
                                    </div>
                                    <label class="font-md">Foto gallerij</label>
                                    <p class="pretty-text">Klik op <b>media toevoegen</b> > <b>Galerij aanmaken</b> >
                                        <b>e.v.t. bestanden uploaden</b> > <b>Bestanden selecteren</b></p>
									<?php
									$content  = '';
									$editor   = 'verhuur_available1';
									$settings = array(
										'textarea_name' => 'verhuur_available[0][images]',
										'textarea_rows' => 8,
										'media_buttons' => true,
										'teeny'         => true,
									);
									wp_editor( $content, $editor, $settings ); ?>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <p class="btn btn-danger delete-input" href="" role="">
                                <span class="glyphicon glyphicon-minus"></span>
                                </p>
                            </div>
                        </div>
                    </section>
				<?php } ?>
            </section>
            <button class="btn btn-primary add-input" href="" role="button">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            </form>
        </div>
    </div>
    </form>
    <script>
        $('.add-input').click(function () {
            var countInput = <?php echo $key + 1?>;
            var inputNames = ['title', 'beschikbaar'];
            var textareaNames = ['beschrijving', 'images'];

            var $htmlStr = $(".dynamic-input:first").clone();
            $htmlStr.find('input').each(function (i) {
                var name = inputNames[i];
                console.log(i);
                $(this).attr('name', 'verhuur_available[' + countInput + '][' + name + ']');
                $(this).attr('value', '');
            });


            $htmlStr.find('textarea').each(function (i) {
                var name = textareaNames[i];
                $(this).attr('name', 'verhuur_available[' + countInput + '][' + name + ']');
                $(this).attr('value', '');
            });
            $htmlStr.appendTo('#dynamic_verhuur_metabox_wrapper');

        });
        $(document).on('click', '.delete-input', function () {
            $(this).closest('.dynamic-input').remove();
        });

    </script>
</section>
