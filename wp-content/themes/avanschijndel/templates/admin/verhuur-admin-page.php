<section class="admin-custom-metabox ">
	<div class="row">
		<div class="col-md-12">
			<form>
				<div class="form-group">
					<label class="font-md">Plaats</label>
					<input type="text" class="form-control" name="verhuur_place" maxlength="15"
					       value="<?php if ( ! empty ( $dwwp_stored_meta['verhuur_place'] ) ) {
						       echo esc_attr( $dwwp_stored_meta['verhuur_place'][0] );
					       } ?>">
				</div>
				<div class="form-group">
					<label class="font-md">Beschrijving</label>
					<textarea class="form-control" name="verhuur_description"><?php if ( ! empty ( $dwwp_stored_meta['verhuur_description'] ) ) {
	                        echo esc_attr( $dwwp_stored_meta['verhuur_description'][0] );
                        } ?></textarea>
				</div>
				<div class="form-group">
					<label class="font-md">Meer info Naam</label>
					<input type="text" class="form-control" name="verhuur_more_info_name" maxlength="30"
					       value="<?php if ( ! empty ( $dwwp_stored_meta['verhuur_more_info_name'] ) ) {
						       echo esc_attr( $dwwp_stored_meta['verhuur_more_info_name'][0] );
					       } ?>">
				</div>
				<div class="form-group">
					<label class="font-md">Meer info link</label>
					<p class="pretty-text">Volledige url</p>
					<input type="text" class="form-control" name="verhuur_more_info_link" maxlength="50"
					       value="<?php if ( ! empty ( $dwwp_stored_meta['verhuur_more_info_link'] ) ) {
						       echo esc_attr( $dwwp_stored_meta['verhuur_more_info_link'][0] );
					       } ?>">
				</div>
				<div class="form-group">
					<label class="font-md">Foto gallerij</label>
					<p class="pretty-text">Klik op <b>media toevoegen</b> > <b>Galerij aanmaken</b> > <b>e.v.t. bestanden uploaden</b> > <b>Bestanden selecteren</b></p>
					<?php
					$content  = get_post_meta( $post->ID, 'verhuur_images', true );
					$editor   = 'verhuur_images';
					$settings = array(
						'textarea_rows' => 8,
						'media_buttons' => true,
						'quicktags'     => false,
					);
					wp_editor( $content, $editor, $settings ); ?>
				</div>
		</div>
	</div>
</section>
