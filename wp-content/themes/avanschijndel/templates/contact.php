<?php
/**
 * Template Name: contact
 */
get_header(); ?>
    <div id="primary" class="content-area contact-page">
        <main id="main" class="site-main">
            <div class="container-fluid fluid-block">
                <div class="container">
                    <div class="col-md-8 col-xs-12 contact-wrapper">
                        <h1 class="main-title">Contact</h1>
                        <p class="font-sm pretty-text">
							<?php if ( get_field( 'tekst' ) ) {
								echo get_field( 'tekst' );
							} ?>
                        </p>
                        <div class="contact-form">
							<?php if ( get_field( 'contact_form' ) ) {
								echo get_field( 'contact_form' );
							} ?>
                        </div>
                    </div>
                    <div class="col-md-offset-1 col-xs-offset-0 col-md-3 col-xs-12 contact-info-wrapper">
                        <h2 class="sub-title-grey font-lg-md bold">Contact info</h2>
						<?php if ( get_field( 'contact-info' ) ) {
							echo get_field( 'contact-info' );
						} ?>
                    </div>
                </div>
            </div>
            <div class="container-fluid fluid-block maps-wrapper">
                <div id="map"></div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->
<?php
get_footer();