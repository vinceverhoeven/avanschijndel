<?php
/**
 * Template Name: nieuws
 */
get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main news-page">
            <div class="fluid-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="main-title font-lg"><?php echo ucfirst( basename( get_permalink() ) ); ?></h1>
                        </div>
                    </div>
                    <div class="row">

						<?php
						$args = array(
							'post_type' => 'post'
						);

						$post_query = new WP_Query( $args );
						if ( $post_query->have_posts() ) {
							while ( $post_query->have_posts() ) {
								$post_query->the_post();
								?>
                                <div class="tile col-sm-6 col-md-4 col-xs-12">
                                    <div class="thumbnail">
	                                    <?php if ( has_post_thumbnail() ) :
		                                    the_post_thumbnail();
	                                    else:?>
                                            <img src="http://via.placeholder.com/350x150">
	                                    <?php endif; ?>
                                        <div class="caption">
                                            <header>
                                                <section class="date-wrapper">
                                                    <p class="date bold">Gepost op:</p>
                                                    <time class="updated bold" datetime="<?php echo get_the_date( 'l j F Y', $post_id ); ?>">
														<?php echo get_the_date( 'l j F Y', $post_id ); ?>
                                                    </time>
                                                    <h3 class="sub-title-prim font-lg-md">
                                                        <?php the_title(); ?> </h3>
                                                </section>
                                            </header>
                                            <p class="content pretty-text font-sm">
												<?php
                                                $content = get_the_content( );
												echo mb_strimwidth($content, 0, 100, "..");
												?>
                                            </p>
                                            <div class="tile-button-wrapper">
                                                <a href="<?php the_permalink() ?>" class="main-button light right"
                                                   role="button">Lees meer</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							<?php }
						} ?>
                    </div>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->
<?php
get_footer();