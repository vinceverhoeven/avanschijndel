<?php
/**
 * Template Name: nieuws-detail
 * Template Post Type: post
 */
get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main news-detail-page">
            <div class="fluid-block no-mobile-pad ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8">
							<?php if ( have_posts() ) : while ( have_posts() ) :
							the_post(); ?>
                            <article>
								<?php the_post_thumbnail(); ?>
                                <header>
                                    <h1 class="main-title"><?php the_title() ?></h1>
                                    <time class="updated" datetime="2010-08-07 11:11:03-0400">
										<?php echo get_the_date( 'l j F Y', $post_id ); ?>
                                    </time>
                                    <p class="author"><?php the_author() ?></p>
                                </header>
                                <ol class="breadcrumb">
                                    <li><a href="nieuws">Nieuws</a></li>
                                    <li class="active"><?php echo mb_strimwidth( get_the_title(), 0, 30, ".." ); ?></a></li>
                                </ol>
                                <div class="content pretty-text font-md">
									<?php echo $post->post_content; ?>
                                </div>
								<?php endwhile; else : ?>
                                    <p class="content pretty-text font-md"><?php esc_html_e( 'Geen content gevonden.' ); ?></p>
								<?php endif; ?>
                            </article>
							<?php include_once( 'components/social-shares.php' ) ?>
                        </div>
                    </div>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->
<?php
get_footer();