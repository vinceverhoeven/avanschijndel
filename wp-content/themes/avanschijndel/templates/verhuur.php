<?php
/**
 * Template Name: verhuur
 */
get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main verhuur-page">
            <section class="hero-image-wrapper bg-c-g">
				<?php the_post_thumbnail(); ?>
            </section>
            <div class="fluid-block">
                <div class="container container-sm-full">
                    <div class="col-md-3 col-sm-3 col-xs-12 sidebar">
						<?php include( 'components/sidebar.php' ) ?>
                    </div>
					<?php
					global $post;
					$post_slug = $post->post_name;
					?>
                    <div class="col-md-9 col-sm-9 col-xs-12 bg-c-w">
                        <div class="row">
                            <section class="col-md-12">
                                <h1 class="main-title font-lg">Verhuur <?php echo $post_slug ?></h1>
                            </section>
                        </div>
						<?php
						// show status

						/**
						 * $post_slug (page name slug) has to be same as taxonomy slug!
						 */
						$args = array(
							'post_type'      => 'verhuur',
							'post_status'    => 'publish',
							'posts_per_page' => - 1,
							'tax_query'      => array(
								array(
									'taxonomy' => 'Sector',
									'field'    => 'slug',
									'terms'    => $post_slug,
								),
							)
						);

						$posts = new WP_Query( $args );
						if ( $posts->have_posts() ) {
							while ( $posts->have_posts() ) : $posts->the_post();
								global $post;
								$postMeta = get_post_meta( get_the_ID() );
								$terms    = wp_get_post_terms( get_the_ID(), 'Status' );
								?>
                                <div class="row">
                                    <div class="col-md-12 tile-horizontal">
                                        <section class="tile-horizontal-block">
                                            <a href="<?php the_permalink() ?>">
                                                <div class="col-md-5 col-sm-5 col-xs-12 image-wrapper">
													<?php
													if ( has_post_thumbnail() ) :
														the_post_thumbnail();
													else:?>
                                                        <img src="http://via.placeholder.com/350x150">
													<?php endif; ?>
													<?php
													if ( $terms[0]->name ) : ?>
                                                        <label class="status-tile">
															<?php echo $terms[0]->name; ?>
                                                        </label>
													<?php endif; ?>
                                                </div>
                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <header>
                                                        <h2 class="sub-title-prim font-lg"> <?php echo mb_strimwidth( get_the_title(), 0, 50, ".." ); ?></h2>
                                                        <span class="label label-primary font-sm"><?php echo mb_strimwidth( $postMeta['verhuur_place'][0], 0, 15, ".." ); ?></span>
                                                    </header>
                                                    <p class="pretty-text font-md content">
														<?php
														$content = $postMeta['verhuur_description'][0];
														echo mb_strimwidth( $content, 0, 150, ".." );
														?>
                                                    </p>
                                                    <a href="<?php the_permalink() ?>" class=" light right"
                                                       role="button">Lees
                                                        meer</a>
                                                </div>
                                            </a>
                                        </section>
                                    </div>
                                </div>
								<?php
							endwhile;
						}
						wp_reset_query();
						?>

                    </div>
                    <div class="col-xs-12 sidebar">
                        <div id="mobile-sidebar-paste">
                        </div>
                    </div>
                </div>
        </main><!-- #main -->
    </div><!-- #primary -->
<?php
get_footer();
