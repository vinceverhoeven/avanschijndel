<?php
/**
 * Template Name: Verkoop
 * Template Post Type: verkoop
 */
get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main verkoop-detail-page">
            <section class="hero-image-wrapper bg-c-g">
				<?php the_post_thumbnail(); ?>
            </section>
            <div class="fluid-block">
                <div class="container container-sm-full">
                    <div class="col-md-3 col-sm-3 col-xs-12 sidebar">
						<?php include( 'components/sidebar.php' ) ?>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 bg-c-w">
                        <div class="row">
                            <div class="col-md-12">
								<?php if ( have_posts() ) : ?>
								<?php while ( have_posts() ) :
								the_post();

								$postMeta = get_post_meta( get_the_ID() );
								$sector   = avanschijndel_get_single_taxonomy( get_the_ID(), 'Sector' );
								$status   = avanschijndel_get_single_taxonomy( get_the_ID(), 'Status' );
								?>
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="<?php echo avanschijndel_get_breadcrumb_link( 'Verkoop' ) . $sector[0]->slug; ?>"><?php echo $sector[0]->slug ?></a>
                                    </li>
                                    <li class="active"><?php echo mb_strimwidth( get_the_title(), 0, 30, ".." ); ?></a></li>
                                </ol>
                                <h1 class="main-title font-lg"><?php the_title() ?></h1>
                                <span class="label label-primary font-sm"><?php echo $postMeta['verkoop_place'][0] ?></span>
								<?php if ( ! empty( $status ) ): ?>
                                    <span class="label font-c-black label-primary font-sm bg-accent">
	                                <?php echo $status[0]->slug; ?>
                                </span>
								<?php endif; ?>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 featured-image">
								<?php if ( has_post_thumbnail() ) :
									the_post_thumbnail();
								else:?>
                                    <img src="http://via.placeholder.com/350x150">
								<?php endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="pretty-text font-md">
									<?php echo $postMeta['verkoop_description'][0]; ?>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <hr/>
								<?php if ( $postMeta["verkoop_more_info_name"][0] && $postMeta["verkoop_more_info_link"][0] ): ?>
                                    <p class="pretty-text font-md">Voor meer informatie: <span
                                                class=" font-c-black">
                                        <?php echo $postMeta["verkoop_more_info_name"][0] ?>
                                    </span>
                                    </p>
                                    <a href="<?php echo $postMeta["verkoop_more_info_link"][0] ?>" target="_blank">Ga
                                        naar
                                        website</a>
								<?php else: ?>
                                    <section class="more-info">
                                        <p class="pretty-text font-md inline">Voor meer informatie:</p>
                                        <a class="font-md inline" href="<?php echo get_site_url() . '/contact'; ?>">Neem
                                            contact op</a>
                                    </section>
								<?php endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <section class="image-wrapper-lightbox">
								<?php if ( ! empty( $postMeta["verkoop_images"][0] ) ) :
									preg_match_all( '!\d+!', $postMeta["verkoop_images"][0], $matches );
									foreach ( $matches[0] as $key => $value ) : ?>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="thumbnail">
                                                <a href="<?php echo wp_get_attachment_url( $value ) ?>"
                                                   data-lightbox="roadtrip">
                                                    <img src="<?php echo wp_get_attachment_url( $value ) ?>">
                                                </a>
                                            </div>
                                        </div>
									<?php endforeach;
								endif; ?>
                            </section>
                        </div>
						<?php endwhile; ?>
						<?php endif; ?>
                    </div>
                    <div class="col-xs-12 sidebar">
                        <div id="mobile-sidebar-paste">
                        </div>
                    </div>
                </div>
        </main><!-- #main -->
    </div><!-- #primary -->
<?php
get_footer();