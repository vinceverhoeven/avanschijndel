<?php
/**
 * Template Name: Verhuur
 * Template Post Type: verhuur
 */
get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main verhuur-detail-page">
            <section class="hero-image-wrapper bg-c-g">
				<?php the_post_thumbnail(); ?>
            </section>
            <div class="fluid-block">
                <div class="container container-sm-full">
                    <div class="col-md-3 col-sm-3 col-xs-12 sidebar">
						<?php include( 'components/sidebar.php' ) ?>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 bg-c-w">
                        <div class="row">
                            <div class="col-md-12">
								<?php if ( have_posts() ) : ?>
								<?php while ( have_posts() ) :
								the_post();

								$postMeta = get_post_meta( get_the_ID() );
								$sector   = avanschijndel_get_single_taxonomy( get_the_ID(), 'Sector' );
								$status   = avanschijndel_get_single_taxonomy( get_the_ID(), 'Status' );
								?>
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="<?php echo avanschijndel_get_breadcrumb_link( 'verhuur' ) . $sector[0]->slug; ?>"><?php echo $sector[0]->slug ?></a>
                                    </li>
                                    <li class="active"><?php echo mb_strimwidth( get_the_title(), 0, 30, ".." ); ?></a></li>
                                </ol>
                                <h1 class="main-title font-lg"><?php the_title() ?></h1>
                                <span class="label label-primary font-sm"><?php echo $postMeta['verhuur_place'][0] ?></span>
								<?php if ( ! empty( $status ) ): ?>
                                    <span class="label font-c-black label-primary font-sm bg-accent">
	                                <?php echo $status[0]->slug; ?>
                                </span>
								<?php endif; ?>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 featured-image">
								<?php if ( has_post_thumbnail() ) :
									the_post_thumbnail();
								else:?>
                                    <img src="http://via.placeholder.com/350x150">
								<?php endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="pretty-text font-md">
									<?php echo $postMeta['verhuur_description'][0]; ?>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <hr/>
								<?php if ( $postMeta["verhuur_more_info_name"][0] && $postMeta["verhuur_more_info_link"][0] ): ?>
                                    <p class="pretty-text font-md font-c-prim">Meer informatie: <span
                                                class=" font-c-black">
                                        <?php echo $postMeta["verhuur_more_info_name"][0] ?>
                                    </span>
                                    </p>
                                    <a href="<?php echo $postMeta["verhuur_more_info_link"][0] ?>" target="_blank">Ga
                                        naar
                                        website</a>
								<?php else: ?>
                                    <section class="more-info">
                                        <p class="pretty-text font-md font-c-black inline">Meer informatie:</p>
                                        <a class="font-md inline" href="<?php echo get_site_url() . '/contact'; ?>">Neem
                                            contact op</a>
                                    </section>
								<?php endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <section class="image-wrapper-lightbox">
								<?php if ( ! empty( $postMeta["verhuur_images"][0] ) ) :
									preg_match_all( '!\d+!', $postMeta["verhuur_images"][0], $matches );
									foreach ( $matches[0] as $key => $value ) : ?>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="thumbnail">
                                                <a href="<?php echo wp_get_attachment_url( $value ) ?>"
                                                   data-lightbox="roadtrip">
                                                    <img src="<?php echo wp_get_attachment_url( $value ) ?>">
                                                </a>
                                            </div>
                                        </div>
									<?php endforeach;
								endif; ?>
                            </section>
                            <hr>
                        </div>
						<?php
						$available_locations = unserialize( $postMeta['verhuur_available'][0] );
						if ( $available_locations ): ?>
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="sub-title-prim font-lg-md bold">Nog beschikbaar</h2>
                            </div>
							<?php
							foreach ( $available_locations as $available_location ): ?>
                                <div class="col-md-12 tile-horizontal">
                                    <section class="tile-horizontal-block">
                                        <div class="col-md-5 col-sm-5 col-xs-12 image-wrapper">
                                            <div class="owl-carousel owl-theme">
												<?php if ( ! empty( $available_location["images"] ) ) :
													preg_match_all( '!\d+!', $available_location["images"], $matches );
													foreach ( $matches[0] as $key => $value ) : ?>
                                                        <div class="item">
                                                            <a href="<?php echo wp_get_attachment_url( $value ) ?>"
                                                               data-lightbox="roadtrip">
                                                                <img src="<?php echo wp_get_attachment_url( $value ) ?>">
                                                            </a>
                                                        </div>
													<?php endforeach; ?>
												<?php else: ?>
                                                    <div class="item">
                                                        <a href="http://via.placeholder.com/350x150"
                                                           data-lightbox="image-1">
                                                            <img src="http://via.placeholder.com/350x150">
                                                        </a>
                                                    </div>
												<?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                            <header>
                                                <time class="font-sm bold"><?php echo mb_strimwidth( $available_location["beschikbaar"], 0, 40, ".." ); ?></time>
                                                <h2 class="sub-title-prim font-lg bold"><?php echo mb_strimwidth( $available_location["title"], 0, 30, ".." ); ?></h2>
                                            </header>
                                            <p class="pretty-text font-sm content">
												<?php echo mb_strimwidth( $available_location["beschrijving"], 0, 200, ".." ); ?>
                                            </p>
                                        </div>
                                        <a class="main-button light font-md right bottom"
                                           href="<?php echo get_site_url() . '/contact'; ?>">Contact</a>
                                    </section>
                                </div>
							<?php endforeach; ?>
							<?php endif; ?>
                        </div>
						<?php endwhile; ?>
						<?php endif; ?>
                    </div>
                    <div class="col-xs-12 sidebar">
                        <div id="mobile-sidebar-paste">
                        </div>
                    </div>
                </div>
        </main><!-- #main -->
    </div><!-- #primary -->
<?php
get_footer();