<section id="mobile-sidebar-cut2">
    <section id="mobile-sidebar-cut">
        <div class="row">
            <div class="col col-md-12">
                <section class="sidebar-nav ">
                    <h2 class="font-lg bold title">Sectoren</h2>
                    <ul class="content font-md">
						<?php
						if ( $post->ancestors ) {
							$ancestors = end( $post->ancestors );
							$children  = wp_list_pages( "title_li=&child_of=" . $ancestors . "&echo=0" );
						} else {
							$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
							if ( strpos( $url, 'verkopen' ) !== false ) {
								$children = wp_list_pages( "title_li=&child_of=6&echo=0" );
							}
							if ( strpos( $url, 'verhuren' ) !== false ) {
								$children = wp_list_pages( "title_li=&child_of=8&echo=0" );
							}
						}
						if ( $children ) {
							?>
							<?php echo $children; ?>
						<?php } ?>
                    </ul>
                </section>
            </div>
        </div>
        <div class="row main-row">
            <div class="col col-md-12 news-wrapper sidebar-news">
                <h2 class="font-lg-md title">Het laatste nieuws</h2>
				<?php
				$args = array(
					'post_type'      => 'post',
					'posts_per_page' => 2,
					'no_found_rows'  => true,
				);

				$news = new WP_Query( $args );
				if ( $news->have_posts() ) {
					while ( $news->have_posts() ) : $news->the_post();
						?>
                        <a href="<?php the_permalink() ?>">
                            <div class="col-md-11 col-xs-11 news-block">
                                <!-- Right-aligned -->
                                <div class="media">
                                    <div class="media-body">
                                        <h4 class="media-heading font-sm"><?php
											$title = get_the_title();
											echo mb_strimwidth( $title, 0, 25, ".." ); ?></h4>
                                        <hr/>
                                        <date class="updated bold"
                                              datetime="<?php echo get_the_date( 'l j F Y', $post_id ); ?>">
											<?php echo get_the_date( 'l j F Y', $post_id ); ?>
                                        </date>
                                    </div>
                                </div>
                            </div>
                        </a>
						<?php
					endwhile;
				}
				wp_reset_query();
				?>
                <a class="main-button dark font-md" href="<?php echo get_site_url() . '/nieuws'; ?>">Lees meer</a>
            </div>
        </div>
        <div class="row main-row">
            <div class="col col-md-12 sidebar-reserve">
                <h2 class="font-lg-md sub-title-prim">Voorinschrijven</h2>
                <p class="font-sm">Heeft u interesse dan kunt u zich alvast voor inschrijven.</p>
                <a class="main-button light font-md" href="<?php echo get_site_url() . '/contact'; ?>">Contact</a>
            </div>
        </div>
    </section>
</section>


