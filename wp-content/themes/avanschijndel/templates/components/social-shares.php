<section class="social-shares">
    <p class="font-md bold">Deel dit artikel</p>
    <hr/>
	<?php
	global $post;
	if ( ! has_post_thumbnail( $post->ID ) ) {
		$thumbnail_src = get_template_directory_uri() . "/images/schijndellogo.png";

	} else {
		$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
	}
	$content     = strip_tags( $post->post_content );
	$title       = $post->post_title;
	$actual_link = ( isset( $_SERVER['HTTPS'] ) ? "https" : "http" ) . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	?>
    <section class="share-buttons-wrapper">
        <a class="linkedin customer share"
           href="<?php echo 'https://www.linkedin.com/shareArticle?mini=true&url=' . $actual_link . '&amp;text=' . $content; ?>"
           title="<?php echo $title ?>" target="_blank"></a>
        <a class="twitter customer share"
           href="<?php echo 'https://twitter.com/share?url=' . $actual_link . '&amp;text=' . $content;; ?>"
           title="<?php echo $title ?>" target="_blank"></a>
        <a class="facebook customer share"
           href="<?php echo "https://www.facebook.com/sharer.php?u=" . $actual_link; ?>"
           title="<?php echo $title ?>" target="_blank"></a>
    </section>
</section>
