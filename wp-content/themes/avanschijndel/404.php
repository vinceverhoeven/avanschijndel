<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package avanschijndel
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main not-found-page">
            <section class="error-404 not-found container">
                <div class="col-md-12 text-center">
                    <header class="page-header">
                        <h1 class="page-title"><?php esc_html_e( 'Oops! Deze pagina bestaat niet.', 'avanschijndel' ); ?></h1>
                    </header><!-- .page-header -->
                    <div class="page-content">
                    </div><!-- .page-content -->
                </div>
            </section><!-- .error-404 -->

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
