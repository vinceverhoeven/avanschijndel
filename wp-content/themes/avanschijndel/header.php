<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package avanschijndel
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php wp_head(); ?>
</head>

<body>
<header class="main-nav">
    <nav class="navbar navbar-default">
        <section class="top-navbar">
            <div class="container">
                <section class="bel-ons-wrapper">
                    <p class="font-sm"><span>Bel ons </span>0492 79 28 99</p>
                </section>
                <section class="mail-ons-wrapper">
                    <p class="font-sm"><span>Mail ons </span><a href="mailto:info@vanschijndelbeheer.nl" target="_top">info@vanschijndelbeheer.nl</a>
                    </p>
                </section>
            </div>
        </section>
        <div class="container">
            <div class="row content-header">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-target="#bs-example-navbar-collapse-1"
                            aria-expanded="false">
                        <span class="icon-bar icon-bar1"></span>
                        <span class="icon-bar icon-bar2"></span>
                        <span class="icon-bar icon-bar3"></span>
                    </button>
					<?php the_custom_logo(); ?>
                </div>
                <div class="collapse navbar-collapse font-md">
					<?php
					wp_nav_menu( array(
						'menu_class'     => 'nav navbar-nav menu navbar-center',
						'theme_location' => 'primary'
					) );
					?>
                </div>
            </div>
    </nav>
    <!--mobile sidenav-->
</header>
<nav class="sidenav">
    <section class="main-sidenav">
		<?php
		wp_nav_menu( array(
			'menu_class'     => 'nav',
			'theme_location' => 'primary'
		) );
		?>
    </section>
</nav>
<div class="sidenav-overlay"></div>



