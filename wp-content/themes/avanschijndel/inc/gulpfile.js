var gulp = require('gulp');
var less = require('gulp-less');
var minifyCSS = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var order = require('gulp-order');

function handleError(err) {
    console.log(err.toString());
}

gulp.task('less', function () {
    return gulp.src('../css/theme.less')  // only compile the entry file
        .pipe(less())
        .pipe(minifyCSS({processImport: false}))
        .pipe(gulp.dest('../css/dist'))
});

gulp.task('js', function () {
    return gulp.src([
        '../js/*.js'
    ])
        .pipe(uglify())
        .pipe(order([
            'infoBox.js',
            'app.js',
            '*.js'
        ]))
        .pipe(concat('le-gulp-javascript.min.js'))
        .pipe(gulp.dest('../js/dist'))
});


gulp.task('watch', function () {
    gulp.watch('../css/**/*.less', ['less']);  // Watch all the .less files, then run the less task
    gulp.watch('../js/*.js', ['js']);
}).on('error', handleError);
;

gulp.task('default', ['watch']); // Default will run the 'entry' watch task