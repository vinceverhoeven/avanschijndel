<?php

function avanschijndel_verhuur_taxonomy_sector() {

	$singular = 'Sector';
	$plural   = 'Sectoren';

	$labels = array(
		'name'                       => $plural,
		'singular_name'              => $singular,
		'search_items'               => 'Search ' . $plural,
		'popular_items'              => 'Popular ' . $plural,
		'all_items'                  => 'All ' . $plural,
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => 'Edit ' . $singular,
		'update_item'                => 'Update ' . $singular,
		'add_new_item'               => 'Add New ' . $singular,
		'new_item_name'              => 'New ' . $singular . ' Name',
		'separate_items_with_commas' => 'Separate ' . $plural . ' with commas',
		'add_or_remove_items'        => 'Add or remove ' . $plural,
		'choose_from_most_used'      => 'Choose from the most used ' . $plural,
		'not_found'                  => 'No ' . $plural . ' found.',
		'menu_name'                  => $plural,
	);

	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array(
			'with_front'   => false,
			'hierarchical' => true
		),
	);

	register_taxonomy( 'Sector', array( 'verkoop', 'verhuur' ), $args );
}

add_action( 'init', 'avanschijndel_verhuur_taxonomy_sector' );

function avanschijndel_verhuur_taxonomy_status() {

	$singular = 'Status';
	$plural   = 'Status';

	$labels = array(
		'name'                       => $plural,
		'singular_name'              => $singular,
		'search_items'               => 'Search ' . $plural,
		'popular_items'              => 'Popular ' . $plural,
		'all_items'                  => 'All ' . $plural,
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => 'Edit ' . $singular,
		'update_item'                => 'Update ' . $singular,
		'add_new_item'               => 'Add New ' . $singular,
		'new_item_name'              => 'New ' . $singular . ' Name',
		'separate_items_with_commas' => 'Separate ' . $plural . ' with commas',
		'add_or_remove_items'        => 'Add or remove ' . $plural,
		'choose_from_most_used'      => 'Choose from the most used ' . $plural,
		'not_found'                  => 'No ' . $plural . ' found.',
		'menu_name'                  => $plural,
	);

	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array(
			'with_front'   => false,
			'hierarchical' => true
		),
	);

	register_taxonomy( 'Status', array( 'verkoop', 'verhuur' ), $args );
}

add_action( 'init', 'avanschijndel_verhuur_taxonomy_status' );

function avanschijndel_verhuur_taxonomy_highlighted() {

	$singular = 'Uitgelicht';
	$plural   = 'Uitgelicht';

	$labels = array(
		'name'                       => $plural,
		'singular_name'              => $singular,
		'search_items'               => 'Search ' . $plural,
		'popular_items'              => 'Popular ' . $plural,
		'all_items'                  => 'All ' . $plural,
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => 'Edit ' . $singular,
		'update_item'                => 'Update ' . $singular,
		'add_new_item'               => 'Add New ' . $singular,
		'new_item_name'              => 'New ' . $singular . ' Name',
		'separate_items_with_commas' => 'Separate ' . $plural . ' with commas',
		'add_or_remove_items'        => 'Add or remove ' . $plural,
		'choose_from_most_used'      => 'Choose from the most used ' . $plural,
		'not_found'                  => 'No ' . $plural . ' found.',
		'menu_name'                  => $plural,
	);

	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array(
			'with_front'   => false,
			'hierarchical' => true
		),
	);

	register_taxonomy( 'Uitgelicht', array( 'verkoop', 'verhuur' ), $args );
}

add_action( 'init', 'avanschijndel_verhuur_taxonomy_highlighted' );