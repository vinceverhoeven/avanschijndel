<?php

/**
 * Add verhuur post type
 */

function avanschijndel_register_verhuur() {

	$singular = __( 'Verhuur' );
	$plural   = __( 'Verhuur' );

	$labels = array(
		'name'         => $plural,
		'singular'     => $singular,
		'add_name'     => 'Voeg nieuwe',
		'add_new_item' => 'Nieuwe ' . $singular,
		'not_found'    => 'Geen verhuur gevonden'
	);

	$args = array(
		'labels'               => $labels,
		'menu_position'        => 5,
		'public'               => true,
		'has_archive'          => false,
		'rewrite'              => array( 'slug' => '/verhuren/%Sector%' ),
		'register_meta_box_cb' => 'avanschijndel_verhuur_add_custom_metabox',
		'supports'             => array(
			'title',
			'thumbnail'
		),
	);
	register_post_type( 'verhuur', $args );
}

add_action( 'init', 'avanschijndel_register_verhuur' );

/**
 * Add verhuur post type metabox
 */
function avanschijndel_verhuur_add_custom_metabox() {

	add_meta_box(
		'verhuur',
		__( 'verhuur' ),
		'avanschijndel_verhuur_meta',
		'verhuur',
		'normal',
		'core'
	);
}

add_action( 'add_meta_boxes', 'avanschijndel_verhuur_add_custom_metabox' );

/**
 * Add verhuur post type metabox content
 */
function avanschijndel_verhuur_meta( $post ) {
	wp_enqueue_style( 'theme.less', get_template_directory_uri() . '/css/dist/theme.css' );

	wp_nonce_field( basename( __FILE__ ), 'avanschijndel_verhuur_nonce' );
	$dwwp_stored_meta = get_post_meta( $post->ID );
	include( get_template_directory() . '/templates/admin/verhuur-admin-page.php' );
}

/**
 * Add verhuur post type metabox
 */
function avanschijndel_verhuur_add_custom_metabox_dynamic() {

	add_meta_box(
		'verhuur_dynamic',
		__( 'Extra locaties' ),
		'avanschijndel_verhuur_meta_dynamic',
		'verhuur',
		'normal',
		'core'
	);
}

add_action( 'add_meta_boxes', 'avanschijndel_verhuur_add_custom_metabox_dynamic' );
/**
 * Add verhuur post type dynamic metabox content
 */
function avanschijndel_verhuur_meta_dynamic( $post ) {
	wp_nonce_field( basename( __FILE__ ), 'avanschijndel_verhuur_nonce' );
	$metadata = get_post_meta( $post->ID, 'verhuur_available', true );
	include( get_template_directory() . '/templates/admin/verhuur-dynamic-admin-page.php' );
}


/**
 * Add verhuur save
 */
function avanschijndel_verhuur_meta_save( $post_id ) {
	$is_autosave    = wp_is_post_autosave( $post_id );
	$is_revision    = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST['dwwp_jobs_nonce'] ) && wp_verify_nonce( $_POST['avanschijndel_verhuur_nonce'], basename( __FILE__ ) ) ) ? 'true' : 'false';
	// Exits script depending on save status
	if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
		echo 'not valid';

		return;
	}
	if ( isset( $_POST['verhuur_place'] ) ) {
		update_post_meta( $post_id, 'verhuur_place', sanitize_text_field( $_POST['verhuur_place'] ) );
	}
	if ( isset( $_POST['verhuur_description'] ) ) {
		update_post_meta( $post_id, 'verhuur_description', sanitize_text_field( $_POST['verhuur_description'] ) );
	}
	if ( isset( $_POST['verhuur_more_info_name'] ) ) {
		update_post_meta( $post_id, 'verhuur_more_info_name', sanitize_text_field( $_POST['verhuur_more_info_name'] ) );
	}
	if ( isset( $_POST['verhuur_more_info_link'] ) ) {
		update_post_meta( $post_id, 'verhuur_more_info_link', sanitize_text_field( $_POST['verhuur_more_info_link'] ) );
	}
	if ( isset( $_POST['verhuur_images'] ) ) {
		update_post_meta( $post_id, 'verhuur_images', sanitize_text_field( $_POST['verhuur_images'] ) );
	}

	if ( isset( $_POST['verhuur_available'] ) ) {
		$field_values_array = $_POST['verhuur_available'];
		update_post_meta( $post_id, 'verhuur_available', $field_values_array );
	}else{
		delete_post_meta($post_id, 'verhuur_available');
	}
}

add_action( 'save_post', 'avanschijndel_verhuur_meta_save' );
add_filter( 'wp_default_editor', create_function('', 'return "tinymce";') );