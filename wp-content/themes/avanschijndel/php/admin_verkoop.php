<?php

/**
 * Add verkoop post type
 */

function avanschijndel_register_verkoop() {

	$singular = __( 'Verkoop' );
	$plural   = __( 'Verkoop' );

	$labels = array(
		'name'         => $plural,
		'singular'     => $singular,
		'add_name'     => 'Voeg nieuwe',
		'add_new_item' => 'Nieuwe ' . $singular,
		'not_found'    => 'Geen verkoop gevonden'
	);

	$args = array(
		'labels'               => $labels,
		'menu_position'        => 5,
		'public' => true,
		'has_archive' => false,
		'rewrite' => array('slug' => '/verkopen/%Sector%'),
		'register_meta_box_cb' => 'avanschijndel_verkoop_add_custom_metabox',
		'supports'             => array(
			'title',
			'thumbnail'
		),
	);
	register_post_type( 'verkoop', $args );
}
add_action( 'init', 'avanschijndel_register_verkoop' );

/**
 * Add verkoop post type metabox
 */
function avanschijndel_verkoop_add_custom_metabox() {

	add_meta_box(
		'Verkoop',
		__( 'Verkoop' ),
		'avanschijndel_verkoop_meta',
		'verkoop',
		'normal',
		'core'
	);
}

add_action( 'add_meta_boxes', 'avanschijndel_verkoop_add_custom_metabox' );

/**
 * Add verkoop post type metabox content
 */
function avanschijndel_verkoop_meta( $post ) {
	wp_enqueue_style( 'theme.less', get_template_directory_uri() . '/css/dist/theme.css' );

	wp_nonce_field( basename( __FILE__ ), 'avanschijndel_verkoop_nonce' );
	$dwwp_stored_meta = get_post_meta( $post->ID );
	include( get_template_directory() . '/templates/admin/verkoop-admin-page.php' );
}

add_action( 'init', 'avanschijndel_register_verkoop' );

/**
 * Add verkoop save
 */
function avanschijndel_verkoop_meta_save( $post_id ) {
	// Checks save status
	$is_autosave    = wp_is_post_autosave( $post_id );
	$is_revision    = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST['dwwp_jobs_nonce'] ) && wp_verify_nonce( $_POST['avanschijndel_verkoop_nonce'], basename( __FILE__ ) ) ) ? 'true' : 'false';
	// Exits script depending on save status
	if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
		echo 'not valid';

		return;
	}
	if ( isset( $_POST['verkoop_place'] ) ) {
		update_post_meta( $post_id, 'verkoop_place', sanitize_text_field( $_POST['verkoop_place'] ) );
	}
	if ( isset( $_POST['verkoop_description'] ) ) {
		update_post_meta( $post_id, 'verkoop_description', sanitize_text_field( $_POST['verkoop_description'] ) );
	}
	if ( isset( $_POST['verkoop_more_info_name'] ) ) {
		update_post_meta( $post_id, 'verkoop_more_info_name', sanitize_text_field( $_POST['verkoop_more_info_name'] ) );
	}
	if ( isset( $_POST['verkoop_more_info_link'] ) ) {
		update_post_meta( $post_id, 'verkoop_more_info_link', sanitize_text_field( $_POST['verkoop_more_info_link'] ) );
	}
	if ( isset( $_POST['verkoop_images'] ) ) {
		update_post_meta( $post_id, 'verkoop_images', sanitize_text_field( $_POST['verkoop_images'] ) );
	}
}

add_action( 'save_post', 'avanschijndel_verkoop_meta_save' );

