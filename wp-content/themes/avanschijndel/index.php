<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package avanschijndel
 */

get_header(); ?>
    <div id="primary" class="content-area front-page">
        <main id="main" class="site-main">
            <section class="hero-image-wrapper">
				<?php the_post_thumbnail(); ?>
                <div class="hero-image-text-wrapper">
                    <section class="container">
                        <div class="hero-image-text-block col-md-4 col-sm-6 col-xs-10">
                            <p class="font-lg title">
								<?php if ( get_field( 'header_titel' ) ) {
									echo get_field( 'header_titel' );
								} ?>
                            </p>
                            <a class="cta-button" href="<?php echo get_site_url() . '/verhuur/woningen'; ?>">Bekijk huur
                                actueel</a>
                            <a class="cta-button" href="<?php echo get_site_url() . '/verkoop/woningen'; ?>">Bekijk koop
                                actueel</a>
                        </div>
                    </section>
                </div>
            </section>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <h1 class="main-title font-lg">
							<?php if ( get_field( 'titel' ) ) {
								echo get_field( 'titel' );
							} ?>
                        </h1>
						<?php while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                            <section class="font-md main-content">
								<?php the_content(); ?>
                            </section>
						<?php endwhile;
						wp_reset_query(); ?>
                    </div>
                    <div class="col-md-offset-1 col-sm-offset-1 col-sm-5 col-md-5 col-xs-offset-0 col-xs-12 news-wrapper">
                        <h2 class="font-lg title">Het laatste nieuws</h2>

						<?php
						$args = array(
							'post_type'      => 'post',
							'posts_per_page' => 2,
							'no_found_rows'  => true,
						);

						$posts = new WP_Query( $args );
						if ( $posts->have_posts() ) {
							while ( $posts->have_posts() ) : $posts->the_post();
								?>
                                <div class="row">
                                    <div class="col-md-11 col-xs-11 news-block">
                                        <a href="<?php the_permalink() ?>"
                                        <!-- Right-aligned -->
                                        <div class="media">
                                            <div class="media-body">
                                                <h4 class="media-heading font-sm"><?php
													$title = get_the_title();
													echo mb_strimwidth( $title, 0, 40, ".." ); ?></h4>
                                                <hr/>
                                                <date class="updated bold"
                                                      datetime="<?php echo get_the_date( 'l j F Y', $post_id ); ?>">
													<?php echo get_the_date( 'l j F Y', $post_id ); ?>
                                                </date>
                                            </div>
                                        </div>
                                        </a>
                                    </div>
                                </div>
								<?php
							endwhile;
						}
						wp_reset_query();
						?>
                        <a class="main-button dark font-md" href="<?php echo get_site_url() . '/nieuws'; ?>">Lees meer</a>
                    </div>
                </div>
            </div>
            <div class="container-fluid fluid-block no-mobile-pad">
                <div class="container">
                    <h2 class="font-lg sub-title-prim uitgelicht-title">Uitgelichte projecten</h2>
                    <div class="row">
						<?php
						$args  = array(
							'post_type'      => array( 'verkoop', 'verhuur' ),
							'post_status'    => 'publish',
							'posts_per_page' => 3,
							'tax_query'      => array(
								array(
									'taxonomy' => 'Uitgelicht',
									'field'    => 'slug',
									'terms'    => 'uitgelicht',
								),
							)
						);
						$posts = new WP_Query( $args );
						if ( $posts->have_posts() ) {
							while ( $posts->have_posts() ) : $posts->the_post();
								global $post;
								$postMeta = get_post_meta( get_the_ID() );
								$terms    = wp_get_post_terms( get_the_ID(), 'Status' );
								?>

                                <div class="tile col-sm-6 col-md-4 col-xs-12">
                                    <div class="thumbnail">
										<?php if ( has_post_thumbnail() ) :
											the_post_thumbnail();
										else:?>
                                            <img src="http://via.placeholder.com/350x150">
										<?php endif; ?>
										<?php if ( $terms[0]->name ) : ?>
                                            <label class="status-tile">
												<?php echo $terms[0]->name; ?>
                                            </label>
										<?php endif; ?>
                                        <div class="caption">
                                            <h3 class="sub-title-prim font-lg-md"><?php echo mb_strimwidth( get_the_title(), 0, 50, ".." ); ?></h3>
                                            <span class="label label-default font-sm"><?php echo mb_strimwidth( $postMeta[ $post->post_type . '_place' ][0], 0, 15, ".." ); ?></span>
                                            <p class="content pretty-text font-sm">
												<?php
												$content = $postMeta[ $post->post_type . '_description' ][0];
												echo mb_strimwidth( $content, 0, 100, ".." );
												?>
                                            </p>
                                            <div class="tile-button-wrapper">
                                                <a href="<?php the_permalink() ?>" class="main-button light right"
                                                   role="button">Lees
                                                    meer</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<?php
							endwhile;
						}
						wp_reset_query();
						?>
                    </div>
                </div>
            </div>
    </div>
    </main><!-- #main -->
    </div><!-- #primary -->
<?php
get_footer();
