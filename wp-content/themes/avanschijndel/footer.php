<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package avanschijndel
 */

?>
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="sub-title-prim font-lg-md bold">Waarmee kunnen we u helpen?</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
                <h3 class="font-md bold">Onze diensten</h3>
                <ul>
                    <li><a class="font-sm" href="<?php echo get_site_url() . '/verhuur/woningen'; ?>">Verhuur actueel</a></li>
                    <li><a class="font-sm" href="<?php echo get_site_url() . '/verkoop/woningen'; ?>">Verkoop actueel</a></li>
                </ul>
                <h3 class="font-md bold">Volg ons</h3>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
	            <?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
	            <?php dynamic_sidebar( 'third-footer-widget-area' ); ?>
                <ul>
                    <li><a class="font-sm button-skir-read-more" href="<?php echo get_site_url() . '/nieuws'; ?>">Lees meer</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <h3 class="font-md bold">Contact ons</h3>
                <p class="font-md bold font-c-prim">Hoofdkantoor</p>
                <ul>
                    <li><p class="font-sm">Gelind 2, 5421 BN Gemert</p></li>
                    <li><p class="font-sm mail">0492 79 28 99</p></li>
                    <li><a class="font-sm mail" href="mailto:info@vanschijndelbeheer.nl" target="_top">info@vanschijndelbeheer.nl</a></li>
                </ul>
            </div>
        </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
